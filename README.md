1. Logging.log allows one to filter, it contains past logs, and can write to stdout/stderr. Console logging only prints to stderr/stdout. 

Logger:
2019-03-25 15:00:09 INFO edu.baylor.ecs.si.Timer <clinit> starting the app
2019-03-25 15:00:10 INFO edu.baylor.ecs.si.Timer timeMe Calling took: 0
2019-03-25 15:00:10 INFO edu.baylor.ecs.si.Timer timeMe * should take: 0
2019-03-25 15:00:10 FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
2019-03-25 15:00:10 FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
2019-03-25 15:00:11 INFO edu.baylor.ecs.si.Timer timeMe Calling took: 1015
2019-03-25 15:00:11 INFO edu.baylor.ecs.si.Timer timeMe * should take: 1000

Console: 
2019-03-25 15:00:09 INFO edu.baylor.ecs.si.Timer <clinit> starting the app
2019-03-25 15:00:10 INFO edu.baylor.ecs.si.Timer timeMe Calling took: 0
2019-03-25 15:00:10 INFO edu.baylor.ecs.si.Timer timeMe * should take: 0

2. The Finer line comes from the JUnit class, specifically from the condition evaluator.
3. It allows you to test multiple exceptions within the same test. It tests that an exception is thrown.
4 - 1. The serialization runtime associates with each serializable class a version number,
 called a serialVersionUID, which is used during deserialization to verify that the sender and receiver of a serialized object 
 have loaded classes for that object that are compatible with respect to serialization. Used to check if it is backwards and forwards compatible, and
 allows two users to use the same object/class
  - 2. Because otherwise the child class would not be created. Constructors are not inherited, so a string is passed to parent constructors.
  - 3. Because we don't want different functionality, and those methods are inherited.
5. Called when the line "Assertions.assertTrue(Timer.timeMe(0) >= 0);" executes. It is a static initializer thats intialized when class is loaded. 
6. Bitbucket Server uses Markdown for formatting text, as specified in CommonMark (with a few extensions). You can use Markdown in the following places:

    any pull request's descriptions or comments, or
    in README files (if they have the .md file extension).

Use Control-Shift-P or Command-Shift-P to preview your markdown.
7. Test failed because timeNow is null and there is no catch block for a TimerException. So when it gets to Finally, a null pointer exception is thrown.
To fix, catch the timerException, and set Long to 0L.
8. The issue is that the timerException is not being caught, so when the finally block is run, it tries to subtract by null.
10. Null pointer is a checked runtime exception, which means it must be handled (caught) or the program stops. timerException is an unchecked exception, so it doesn't have to
be caught for the program to keep running. 
